package ru.t1consulting.vmironova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.vmironova.tm.event.ConsoleEvent;
import ru.t1consulting.vmironova.tm.listener.AbstractListener;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Show application commands.";

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@commandListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        for (@Nullable final AbstractListener listener : listeners) {
            System.out.println(listener.getName());
        }
    }

}
