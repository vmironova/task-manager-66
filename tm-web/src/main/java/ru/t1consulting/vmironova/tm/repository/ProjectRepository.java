package ru.t1consulting.vmironova.tm.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.model.Project;

@Repository
@Scope("prototype")
public interface ProjectRepository extends JpaRepository<Project, String> {

}
