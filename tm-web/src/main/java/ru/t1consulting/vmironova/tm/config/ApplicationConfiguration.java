package ru.t1consulting.vmironova.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1consulting.vmironova.tm")
public class ApplicationConfiguration {

}
