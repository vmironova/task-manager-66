package ru.t1consulting.vmironova.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;

import java.util.List;

public interface ProjectRestEndpointClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/projects";

    static ProjectRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("/findAll")
    List<ProjectDTO> findAll() throws Exception;

    @PostMapping("/add")
    ProjectDTO add(@RequestBody ProjectDTO task) throws Exception;

    @PostMapping("/save")
    ProjectDTO save(@RequestBody ProjectDTO project) throws Exception;

    @GetMapping("/findById/{id}")
    ProjectDTO findById(@PathVariable("id") String id) throws Exception;

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id) throws Exception;

    @GetMapping("/count")
    long count() throws Exception;

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id) throws Exception;

    @PostMapping("/delete")
    void delete(@RequestBody ProjectDTO project) throws Exception;

    @PostMapping("/deleteAll")
    void clear() throws Exception;

}
