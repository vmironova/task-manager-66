package ru.t1consulting.vmironova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.model.Task;
import ru.t1consulting.vmironova.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    long countByUser(@NotNull final User user);

    void deleteByUserAndId(@NotNull final User user, @NotNull final String id);

    @NotNull
    List<Task> findByUser(@NotNull final User user);

    @NotNull
    List<Task> findByUser(@NotNull final User user, @NotNull final Sort sort);

    @NotNull
    Optional<Task> findByUserAndId(@NotNull final User user, @NotNull final String id);

    void deleteByUser(@NotNull final User user);

    @Query("select case when count(c)> 0 then true else false end from Task c where user.id = :userId and id = :id")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

    @NotNull
    List<Task> findByUserAndProjectId(@NotNull final User user, @NotNull final String projectId);

}
