package ru.t1consulting.vmironova.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

}
