package ru.t1consulting.vmironova.tm.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! You do not have permission.");
    }

}
