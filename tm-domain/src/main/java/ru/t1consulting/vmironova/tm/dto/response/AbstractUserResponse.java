package ru.t1consulting.vmironova.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

    public AbstractUserResponse(@Nullable UserDTO user) {
        this.user = user;
    }

}